
from os.path import join
from pygame import display, image, key, time, Surface
from pygame.math import Vector2
from pygame.font import Font
from pygame.mixer import Sound, Channel
from input_state import InputState
from entity import Entity

import pygame
import random

class TimeTracker:
    last_tick: int
    expirations: dict
    def setup(self):
        self.last_tick = time.get_ticks()
        self.expirations = {}
    def step_frame(self) -> float:
        current_tick = time.get_ticks()
        delta = (current_tick - self.last_tick) / 1000.0
        self.last_tick = current_tick
        for id in self.expirations.keys():
            self.expirations[id] -= delta
        return delta
    def expire_in(self, id: int, time: float):
        self.expirations[id] = time
    def get_expired(self) -> list:
        expired = []
        for id, time in self.expirations.items():
            if time <= 0.0:
                expired.append(id)
        for id in expired:
            del self.expirations[id]
        return expired
    def time_since(self, tick: int) -> float:
        return (self.last_tick - tick) / 1000.0


class ResourceLoader:
    root: str = "assets"
    def load_texture(self, name: str) -> Surface:
        path = join(self.root, "%s.png" % name)
        return image.load(path).convert_alpha()
    def load_font(self, name: str, size: int) -> Surface:
        path = join(self.root, "%s.ttf" % name)
        return Font(path, size)
    def load_sfx(self, name: str) -> Sound:
        path = join(self.root, "%s.wav" % name)
        return Sound(path)
    def load_bgm(self, name: str) -> Sound:
        path = join(self.root, "%s.ogg" % name)
        return Sound(path)


class InputReader:
    def read(self) -> InputState:
        state = InputState()
        # event handling, gets all event from the event queue
        for event in pygame.event.get():
            # only do something if the event is of type QUIT
            if event.type == pygame.QUIT:
                # change the value to False, to exit the main loop
                state.exit_requested = True
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    state.exit_requested = True
                if event.key == pygame.K_RETURN:
                    state.pause_requested = True
        pressed = key.get_pressed()
        if pressed[pygame.K_LEFT] or pressed[pygame.K_a]:
            state.steer -= 1.0
        if pressed[pygame.K_RIGHT] or pressed[pygame.K_d]:
            state.steer += 1.0
        state.boosting = pressed[pygame.K_SPACE] or pressed[pygame.K_z]
        state.shooting = pressed[pygame.K_LSHIFT] or pressed[pygame.K_x]
        return state

class EntityKeeper:
    next_id: int = 0

    def generate_id(self) -> int:
        id = self.next_id
        self.next_id += 1
        return id

    def register(self, entities, entity) -> int:
        id = self.generate_id()
        entities[id] = entity
        return id

class EntityBuilder:

    def fighter(self, resource_loader: ResourceLoader) -> Entity:
        fighter = Entity()
        fighter.sprite = resource_loader.load_texture("fighter")
        fighter.offset = Vector2(8, 8)
        fighter.size = 4
        return fighter

    def battleship(self, resource_loader: ResourceLoader) -> Entity:
        battleship = Entity()
        battleship.sprite = resource_loader.load_texture("battleship")
        battleship.offset = Vector2(64, 64)
        battleship.motion = Vector2(0, -20)
        battleship.size = 48
        battleship.hit_points = None
        battleship.debris = True
        return battleship

    def huge_asteroid(self, resource_loader: ResourceLoader) -> Entity:
        asteroid = Entity()
        rotation = random.randrange(4)
        direction = Vector2(0, 1).rotate(random.uniform(-80, 80))
        texture = resource_loader.load_texture("huge_asteroid")
        texture = pygame.transform.rotate(texture, rotation * 90.0)
        asteroid.sprite = texture
        asteroid.offset = Vector2(64, 64)
        asteroid.motion = direction * 40
        asteroid.size = 48
        asteroid.debris = True
        asteroid.hit_points = 20
        return asteroid

    def big_asteroid(self, resource_loader: ResourceLoader) -> Entity:
        asteroid = Entity()
        rotation = random.randrange(4)
        direction = Vector2(0, 1).rotate(random.uniform(-80, 80))
        texture = resource_loader.load_texture("asteroid")
        texture = pygame.transform.rotate(texture, rotation * 90.0)
        asteroid.sprite = texture
        asteroid.offset = Vector2(32, 32)
        asteroid.motion = direction * 80
        asteroid.size = 24
        asteroid.debris = True
        asteroid.hit_points = 8
        return asteroid

    def small_asteroid(self, resource_loader: ResourceLoader) -> Entity:
        asteroid = Entity()
        rotation = random.randrange(4)
        direction = Vector2(0, 1).rotate(random.uniform(-80, 80))
        texture = resource_loader.load_texture("small_asteroid")
        texture = pygame.transform.rotate(texture, rotation * 90.0)
        asteroid.sprite = texture
        asteroid.offset = Vector2(16, 16)
        asteroid.motion = direction * 120
        asteroid.size = 12
        asteroid.debris = True
        asteroid.hit_points = 3
        return asteroid

    def bullet(self, resource_loader: ResourceLoader) -> Entity:
        bullet = Entity()
        texture = resource_loader.load_texture("bullet")
        bullet.sprite = texture
        bullet.offset = Vector2(4, 4)
        bullet.motion = Vector2(0, -800)
        bullet.size = 4
        return bullet

    def missile(self, resource_loader: ResourceLoader) -> Entity:
        missile = Entity()
        texture = resource_loader.load_texture("missile")
        missile.sprite = texture
        missile.offset = Vector2(4, 4)
        missile.size = 4
        missile.debris = True
        return missile

    def boost(self, resource_loader: ResourceLoader) -> Entity:
        boost = Entity()
        texture = resource_loader.load_texture("boost")
        boost.sprite = texture
        boost.offset = Vector2(8, 0)
        boost.size = 0
        return boost

    def particle(self, resource_loader: ResourceLoader) -> Entity:
        particle = Entity()
        texture = resource_loader.load_texture("particle")
        particle.sprite = texture
        particle.offset = Vector2(1.5, 1.5)
        particle.size = 0
        return particle

class ParticleEmitter:
    def emit(self, position: Vector2, builder: EntityBuilder, \
             loader: ResourceLoader) -> list:
        particles = []
        for i in range(6):
            particle = builder.particle(loader)
            particle.position = Vector2(position)
            speed = random.uniform(120, 160)
            particle.motion = Vector2(speed, 0).rotate(random.uniform(0, 360))
            particles.append(particle)
        return particles


class DebrisSpawner:
    BASE_COOLDOWN = 0.0
    spawn_cooldown: float
    spawn_time: float
    max_progress: float
    def setup(self):
        self.spawn_cooldown = 100.0
        self.spawn_time = 0.0
        self.max_progress = 0
    def should_spawn(self, progress: float) -> int:
        spawns = 0
        cooldown = DebrisSpawner.BASE_COOLDOWN + self.spawn_cooldown
        while progress - self.max_progress > cooldown:
            self.max_progress += cooldown
            self.spawn_cooldown *= 0.99
            cooldown = DebrisSpawner.BASE_COOLDOWN + self.spawn_cooldown
            spawns += 1
        return spawns

class PlayerController:
    SHOOT_COOLDOWN = 0.1
    SHOOT_DECELERATION = 0.2
    SPEED = 200.0
    RECOIL = 200.0
    player_id: int = None
    boost_id: int = None
    shoot_time: float
    time_of_death: int = None
    def control(self, player_id: int):
        self.player_id = player_id
        self.shoot_time = 0.0
        self.time_of_death = None
        self.boost_id = None
    def get_player(self, entities: dict) -> Entity:
        if self.player_id in entities:
            return entities[self.player_id]
        else:
            return None
    def move(self, player: Entity, input_state: InputState) -> bool:
        if not input_state.boosting:
            player.motion *= 0.8
            return False
        move_dir = Vector2(input_state.steer / 2.0, -1.0)
        if move_dir.length() > 0.0:
            move_dir = move_dir.normalize()
        speed = PlayerController.SPEED
        if input_state.shooting:
            speed *= PlayerController.SHOOT_DECELERATION
        player.motion = move_dir * speed
        return True
    def shoot(self, player: Entity, input_state: InputState, delta: float) -> bool:
        if input_state.shooting:
            self.shoot_time -= delta
            if self.shoot_time <= 0.0:
                self.shoot_time += PlayerController.SHOOT_COOLDOWN
                recoil_dir = Vector2(0, 1).rotate(random.uniform(-45, 45))
                recoil = recoil_dir * PlayerController.RECOIL
                player.motion += recoil
                return True
        return False
    def shoot_position(self, player: Entity) -> Vector2:
        return player.position + Vector2(0, -16)
    def track_boost(self, boost_id: int):
        self.boost_id = boost_id
    def process_boost(self, entities: dict):
        player = self.get_player(entities)
        if player != None and self.boost_id in entities:
            boost = entities[self.boost_id]
            boost.position = player.position + Vector2(0, 10)
    def check_death(self, entities: dict, tracker: TimeTracker) -> bool:
        if not self.player_id in entities:
            if self.time_of_death == None:
                self.time_of_death = tracker.last_tick
            return tracker.time_since(self.time_of_death) > 1.0
        else:
            return False

class BattleshipController:
    SPEED = 80
    MAX_SPEED = 200
    SHOOT_COOLDOWN = 0.1
    battleship_id: int = None
    time: float = 0.0
    shoot_time: float = 0.0
    def control(self, battleship_id: int):
        self.battleship_id = battleship_id
        self.time = 0.0
        self.shoot_time = 0.0
    def get_battleship(self, entities: dict) -> Entity:
        if self.battleship_id in entities:
            return entities[self.battleship_id]
        else:
            return None
    def accelerate(self, battleship: Entity, delta: float):
        self.time += delta * 0.01
        if self.time > 0.0:
            diff = BattleshipController.MAX_SPEED - BattleshipController.SPEED
            extra = diff * (1.0 - 1.0 / (1.0 + self.time))
            battleship.motion.y = -(BattleshipController.SPEED + extra)
    def shoot(self, battleship: Entity, delta: float) -> bool:
        self.shoot_time -= delta
        if self.shoot_time <= 0.0:
            self.shoot_time += BattleshipController.SHOOT_COOLDOWN
            return True
        return False


class CollisionHandler:
    asteroid_spawns: list = []
    def hit(self, entity: Entity, slowdown: bool = True, amount: int = 1) -> bool:
        if entity.hit_points == None: return False
        entity.hit_points -= amount
        if slowdown:
            entity.slowdown = Entity.SLOWDOWN_TIME
        if entity.hit_points <= 0:
            if entity.debris:
                dirs = [Vector2(0, -1), Vector2(-1, 0), Vector2(1, 0), Vector2(0, 1)]
                if entity.size >= 48.0:
                    for spawn_dir in dirs:
                        position = entity.position + spawn_dir * entity.size
                        self.asteroid_spawns.append(("big", position))
                elif entity.size >= 24.0:
                    for spawn_dir in dirs:
                        position = entity.position + spawn_dir * entity.size
                        self.asteroid_spawns.append(("small", position))
            return True
        return False
    def flush_spawns(self, builder: EntityBuilder, loader: ResourceLoader) -> list:
        spawns = []
        for spawn, pos in self.asteroid_spawns:
            if spawn == "small":
                asteroid = builder.small_asteroid(loader)
            elif spawn == "big":
                asteroid = builder.big_asteroid(loader)
            asteroid.position = pos
            spawns.append(asteroid)
        self.asteroid_spawns = []
        return spawns

class AudioPlayer:
    boost_sfx: Sound = None
    boost_channel: Channel = None
    shoot_sfx: Sound = None
    shoot_channel: Channel = None
    hit_sfx: Sound = None
    bgm: Sound = None

    def setup(self, loader):
        self.boost_channel = Channel(0)
        self.boost_sfx = loader.load_sfx("boost")
        self.shoot_channel = Channel(1)
        self.shoot_sfx = loader.load_sfx("shoot")
        self.hit_sfx = loader.load_sfx("hit")
        self.bgm = loader.load_bgm("bgm")
        Channel(2).play(self.bgm, -1)

    def play_boost(self) -> bool:
        if not self.boost_channel.get_busy():
            self.boost_channel.play(self.boost_sfx)
            return True
        return False

    def play_shoot(self):
        if self.shoot_channel.get_busy():
            self.shoot_channel.stop()
        self.shoot_channel.play(self.shoot_sfx)

    def play_hit(self):
        self.hit_sfx.play()


class Display:
    screen: Surface = None
    scroll: float = 0
    font: Font = None

    def setup(self, caption: str):
        if not pygame.get_init():
            pygame.init()
            display.set_caption(caption)
            self.screen = display.set_mode((360,640))
        else:
            self.screen = display.get_surface()
        self.scroll = 0.0

    def clear(self):
        self.screen.fill((0, 0, 0))

    def set_font(self, font: Font):
        self.font = font

    def draw_texture_at(self, texture: Surface, position: Vector2):
        position += Vector2(0, -self.scroll)
        self.screen.blit(texture, position)

    def draw_text_at(self, text: str, position: Vector2):
        if self.font != None:
            rendered = self.font.render(text, True, (255, 255, 255))
            self.screen.blit(rendered.convert_alpha(), position)

    def drag_scroll(self, y: float) -> float:
        old_scroll = self.scroll
        self.scroll = max(y - 480, min(y - 320, self.scroll))
        return self.scroll - old_scroll

    def refresh(self):
        display.flip()

