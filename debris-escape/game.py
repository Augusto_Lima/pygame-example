
from dataclasses import dataclass, field
from entity import Entity
from services import *
from typing import Dict

import random

@dataclass
class Game:
    # General state
    running: bool                               = False
    paused: bool                                = True
    retry: bool                                 = False
    # Services
    display: Display                            = Display()
    audio_player: AudioPlayer                   = AudioPlayer()
    time_tracker: TimeTracker                   = TimeTracker()
    input_reader: InputReader                   = InputReader()
    resource_loader: ResourceLoader             = ResourceLoader()
    entity_builder: EntityBuilder               = EntityBuilder()
    entity_keeper: EntityKeeper                 = EntityKeeper()
    player_controller: PlayerController         = PlayerController()
    battleship_controller: BattleshipController = BattleshipController()
    debris_spawner: DebrisSpawner               = DebrisSpawner()
    particle_emitter: ParticleEmitter           = ParticleEmitter()
    collision_handler: CollisionHandler         = CollisionHandler()
    # Entities
    entities: Dict[int, Entity]                 = None

    def setup_all(self):
        random.seed()
        self.entities = {}
        self.display.setup("Debris Escape")
        self.time_tracker.setup()
        self.running = True
        font = self.resource_loader.load_font("ShareTechMono-Regular", 16)
        self.display.set_font(font)
        self.audio_player.setup(self.resource_loader)
        self.debris_spawner.setup()

    def create_entities(self):
        fighter = self.entity_builder.fighter(self.resource_loader)
        fighter.position = Vector2(360 / 2, 640 / 2)
        player_id = self.entity_keeper.register(self.entities, fighter)
        self.player_controller.control(player_id)
        battleship = self.entity_builder.battleship(self.resource_loader)
        battleship.position = Vector2(360 / 2, 640)
        battleship_id = self.entity_keeper.register(self.entities, battleship)
        self.battleship_controller.control(battleship_id)

    def process_input(self) -> InputState:
        input_state = self.input_reader.read()
        if input_state.exit_requested:
            self.running = False
        if self.paused:
            if input_state.pause_requested or input_state.boosting:
                self.paused = False
        elif not self.paused and input_state.pause_requested:
            self.paused = True
        return input_state

    def process_control(self, input_state: InputState, delta: float):
        player = self.player_controller.get_player(self.entities)
        if player != None:
            self.player_controller.process_boost(self.entities)
            if self.player_controller.move(player, input_state):
                if self.audio_player.play_boost():
                    boost = self.entity_builder.boost(self.resource_loader)
                    boost.position = player.position + Vector2(0, 10)
                    id = self.entity_keeper.register(self.entities, boost)
                    self.time_tracker.expire_in(id, 0.1)
                    self.player_controller.track_boost(id)
            if self.player_controller.shoot(player, input_state, delta):
                bullet1 = self.entity_builder.bullet(self.resource_loader)
                bullet2 = self.entity_builder.bullet(self.resource_loader)
                bullet1.position = player.position + Vector2(-8, -10)
                bullet2.position = player.position + Vector2(8, -10)
                id1 = self.entity_keeper.register(self.entities, bullet1)
                id2 = self.entity_keeper.register(self.entities, bullet2)
                self.time_tracker.expire_in(id1, 3.0)
                self.time_tracker.expire_in(id2, 3.0)
                self.audio_player.play_shoot()
        battleship = self.battleship_controller.get_battleship(self.entities)
        if battleship != None and self.battleship_controller.shoot(battleship, delta):
            if player != None and battleship.position.y - player.position.y < 100.0:
                missile = self.entity_builder.missile(self.resource_loader)
                missile.position = battleship.position + Vector2(0, 0)
                missile.motion = (player.position - battleship.position).normalize() * 400
                id = self.entity_keeper.register(self.entities, missile)
                self.time_tracker.expire_in(id, 3.0)


    def process_time(self):
        delta = self.time_tracker.step_frame()
        if self.player_controller.check_death(self.entities, self.time_tracker):
            self.retry = True
            self.running = False
        if not self.paused:
            battleship = self.battleship_controller.get_battleship(self.entities)
            if battleship != None:
                self.battleship_controller.accelerate(battleship, delta)
            progress = -self.display.scroll
            spawns = self.debris_spawner.should_spawn(progress)
            for i in range(spawns):
                asteroid = self.entity_builder.huge_asteroid(self.resource_loader)
                top = self.display.scroll
                asteroid.position = Vector2(random.uniform(32, 360-32), top - 80)
                self.entity_keeper.register(self.entities, asteroid)
            for id in self.time_tracker.get_expired():
                if id in self.entities:
                    del self.entities[id]
        return delta

    def process_physics(self, delta: float):
        ids = [id for id in self.entities.keys()]
        destroyed = []
        hits = []
        # Move all entities
        for id, entity in self.entities.items():
            motion = Vector2(entity.motion)
            if entity.slowdown > 0.0:
                if motion.y > 0.0:
                    motion.y *= Entity.SLOWDOWN_FACTOR
                entity.slowdown -= delta
            new_position = entity.position + motion * delta
            if entity.size > 0.0 and \
               (new_position.x - entity.size < 0 or \
                new_position.x + entity.size > 360):
                impact = Vector2(0, new_position.y)
                if new_position.x + entity.size > 360:
                    impact.x = 360
                hits.append(impact)
                if self.collision_handler.hit(entity, False):
                    destroyed.append(id)
                else:
                    entity.motion.x = -entity.motion.x
            else:
                entity.position = new_position
            entity.position.x = max(0, min(360, entity.position.x))
        # Check collisions
        n = len(ids)
        for i in range(n):
            ei = self.entities[ids[i]]
            if ei.size > 0.0:
                for j in range(i+1, n):
                    ej = self.entities[ids[j]]
                    diff = ei.position - ej.position
                    dist = diff.length()
                    if ej.size > 0.0 and ei.debris != ej.debris and dist < (ei.size + ej.size):
                        if self.collision_handler.hit(ei):
                            destroyed.append(ids[i])
                        if self.collision_handler.hit(ej):
                            destroyed.append(ids[j])
                        center = ej.position + diff * ej.size / dist
                        hits.append(center)
        for hit in hits:
            self.audio_player.play_hit()
            particles = self.particle_emitter.emit(hit, self.entity_builder, self.resource_loader)
            for particle in particles:
                id = self.entity_keeper.register(self.entities, particle)
                self.time_tracker.expire_in(id, 0.2)
        # Destroy entities that are too far down
        for id, entity in self.entities.items():
            if entity.position.y > self.display.scroll + 2000 and \
               id != self.battleship_controller.battleship_id:
                destroyed.append(id)
        for id in destroyed:
            if id in self.entities:
                entity = self.entities[id]
                del self.entities[id]
        for entity in self.collision_handler.flush_spawns(self.entity_builder, self.resource_loader):
            self.entity_keeper.register(self.entities, entity)

    def draw_frame(self):
        self.display.clear()
        player = self.player_controller.get_player(self.entities)
        if player != None:
            offset = self.display.drag_scroll(player.position.y)
            # Move all entities to avoid float imprecision
            #for entity in self.entities.values():
            #    entity.position.y -= offset
        for entity in self.entities.values():
            if entity.sprite != None:
                self.display.draw_texture_at(entity.sprite, \
                                             entity.position - entity.offset)
        battleship = self.battleship_controller.get_battleship(self.entities)
        player = self.player_controller.get_player(self.entities)
        if battleship != None and player != None:
            distance = max(0, battleship.position.y - player.position.y)
            self.display.draw_text_at("Battleship distance: %d" % distance, (10, 10))
        self.display.draw_text_at("Progress: %d" % -self.display.scroll, (10, 24))
        self.display.draw_text_at("Entities: %d" % len(self.entities), (10, 36))
        if self.paused:
            self.display.draw_text_at("Press ENTER to play", (360 - 200, 36))
        self.display.refresh()


