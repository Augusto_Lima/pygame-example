
# Exemplo PyGame

Este é o material que fiz para uma aula de *MAC0216 - Técnicas de Programação I*
no curso de graduação de Ciência da Computação da USP.

O objetivo da aula é ensinar, de maneira MUITO superficial e sucinta, os
princípios básicos por trás da programação de jogos digitais. Para isso, a aula
basicamente mostra uma implementação passo-a-passo da base para o jogo Debris
Escape, que também está nesse repositório.

Organização dos arquivos:

```
assets/           - arquivos de imagens, sons e fontes usadas no jogo
debris-escape/    - o jogo completo
gabarito/         - a base do jogo usada na aula
olha-gabarito.py  - script para verificar os passos da aula
```

O resto dos arquivos podem ser ignorados.

## Tecnologias usadas

Para rodar o jogo e o gabarito é preciso ter Python 3 (idealmente 3.8+) e a
PyGame instaladas. Minha recomendação é instalar a PyGame pelo `pip`, caso a
ferramenta lhe seja familiar.

A aula usa a [PyGame](https://www.pygame.org/news) porque era essa edição de
MAC0216 tinha aulas dedicadas a ensinar Python e a aula de programação de jogos
faz parte delas. Existem outras engines de jogos em Python, como a
[Pyglet](http://pyglet.org/) e a [Panda3D](https://www.panda3d.org/), mas
preferi usar a PyGame porquem tem bem mais material sobre ela disponível e,
portanto, seria mais didático. No entanto, não é o que recomendo para de fato
fazer um jogo "de verdade".

## Rodando o código

Para rodar tanto o jogo *Debris Escape* quanto o gabarito, é preciso rodar o
comando abaixo **a partir da pasta raiz desse repositório**:

```bash
$ python debris-escape/main.py # roda o Debris Escape
$ python gabarito/game.py      # roda o gabarito
```

## Passo-a-passo e gabarito

A aula divide o desenvolvimento do jogo em 5 etapas, cada uma com 4 passos. As
linhas de código do gabarito estão marcadas a etapa e o passo que a inserem
pela primeira vez, assim como a etapa e o passo que as removem (possivelmente
para substituí-las por uma linha mais atualizadas). As marcações ficam no final
de cada linha de código e seguem a notação `[XY]` onde X é um número de 1 a 5,
representando a etapa, e Y é um letra de A a D, representando o passo dentro da
etapa. Por exemplo, `[2B]` é o passo B da segunda etapa.

Para faciliar a leitura do gabarito, escrevi um *script* em Python
(`olha-gabarito.py`) que imprime apenas as linhas do gabarito que foram
inseridas ou removidas em uma dada etapa e passo. O *script* pode ser usado da
seguinte forma:

```bash
$ python olha-gabarito.py <etapa-e-passo>
```

Sendo que o parâmetro `<etapa-e-passo>` usa a notação do código. Por exemplo,
a saída do comando `$ python olha-gabarito 3A` imprime as linhas inseridas e
removidas na etapa 3, passo A.

As etapas e passos da aula são os seguintes:

+ Etapa 1: *Game Loop*
  + Passo [1A]: Abrindo uma janela
    + [API da PyGame](https://www.pygame.org/docs/)
  + Passo [1B]: Fazendo a janela não fechar instantaneamente
    + E como fecha agora?
  + Passo [1C]: Fazendo a janela só fechar quando clicar no X
    + [Eventos na PyGame](https://www.pygame.org/docs/ref/event.html)
    + Desacoplar input puro das ações no jogo
  + Passo [1D]: Introduzindo o *Game Loop*
    + Padrões de Projeto e Padrões Arquiteturais
    + Jogos como simulações ou máquinas de estado interativas
    + Arquitura em camadas: sistemas, estado, serviços e módulos baixo-nível
+ Etapa 2: Desenhando na tela
  + Passo [2A]: Desenhando o *sprite* do jogador
    + [Renderização](https://www.pygame.org/docs/ref/display.html) e
      [texturas](https://www.pygame.org/docs/ref/surface.html)
    + [Carregando arquivos de imagem](https://www.pygame.org/docs/ref/image.html)
    + Como ou onde conseguir sprites? E as licenças?
    + Por que o sprite do jogador fica torto?
  + Passo [2B]: Guardando a posição e *offset* do sprite do jogador
    + Offsets e soma de translações
    + Dica: guarde dados no estado do jogo, não deixe para depois!
  + Passo [2C]: Desenhando uma quantidade arbitrária de sprites
    + Generalização e abstração
    + Referências, ponteiros e handles (IDs)
  + Passo [2D]: Desenhando texto
    + Por que escrever para linha de comando não é uma boa ideia em jogos?
    + [Fontes](https://www.pygame.org/docs/ref/font.html) (e licenças!)
+ Etapa 3: Simulando movimento e tempo
  + Passo [3A]: Movendo um sprite sem usar tempo
    + O que acontece se o PC for lento/rápido? Se um frame demorar muito/pouco?
  + Passo [3B]: Movendo um sprite usando tempo
    + [Medindo tempo](https://www.pygame.org/docs/ref/time.html)
    + Simulação em tempo real
  + Passo [3C]: Movendo um sprite usando controles
    + Armazenar movimento no estado do jogo
    + Manter a generalização e abstração de entidades
  + Passo [3D]: Movendo a câmera do jogo
    + Mais somas de translações
    + "Drag margins" para fazer câmeras mais agradáveis
+ Etapa 4: Física e som
  + Passo [4A]: Detectando colisões
    + Intersecção entre formas
    + Antecipar quantidade arbitrária de entidades
    + Evitar colisão duplicada
  + Passo [4B]: Tocando um som
    + O que é som e como um computador toca sons?
    + [Carregando arquivos de som](https://www.pygame.org/docs/ref/mixer.html?highlight=sound#pygame.mixer.Sound)
  + Passo [4C]: Removendo entidades
    + Não remover entidades enquanto itera por elas
    + O jogo não deveria quebrar sem a entidade do jogador!
  + Passo [4D]: Tocando uma música
    + Músicas vs. Efeitos sonoros
    + [Carregando arquivos de música](https://www.pygame.org/docs/ref/music.html)
+ Etapa 5: Fazendo mais mecânicas
  + Passo [5A]: Criando asteróides
    + Poder da abstração em ação!
    + Como impedir asteroides de acertarem uns aos outros?
  + Passo [5B]: Rebatendo asteróides nas laterais
    + "Prever" movimento antes de aplicá-lo
  + Passo [5C]: Atirando projéteis do inimigo
    + Mais abstração!
    + Timers para regular frequência do tiro
    + Som de atirar
  + Passo [5D]: Atirando projéteis do jogador
    + Novo input
    + Apagar projéteis para não vazar memória
    + Embutir um timer especial nas entidades

### Discussão

+ Outros formas de modelar os objetos?

## Referências

+ [Documentação da PyGame](https://www.pygame.org/docs/)
+ [R. Nystrom. "Game Loop" em *Game Programming Patterns*. Genever Bening,
   2014.](http://gameprogrammingpatterns.com/game-loop.html)

## Outras engines

Se você quer uma engine mais completa, com editor visual e funcionalidades de
última geração, minha recomendação pessoal é a
[Godot](https://godotengine.org/), embora seja importante mencionar a
[Unity](https://unity.com/) dado que é a mais famosa do mercado.

